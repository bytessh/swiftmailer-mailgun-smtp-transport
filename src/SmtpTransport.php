<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 3/24/16
 * Time: 5:00 PM
 */

namespace salevan\mailgun;

use Mailgun\Mailgun;
use Swift_Transport;
use Swift_Mime_Message;
use Swift_Events_EventListener;
use Swift_TransportException;


class SmtpTransport implements Swift_Transport
{
    /** @var string $_mailgunDomain */
    private $_mailgunDomain = '';

    /** @var string $_privateApiKey */
    private $_privateApiKey = '';

    /** Connection status */
    protected $_started = true;

    /**
     * Test if this Transport mechanism has started.
     *
     * @return bool
     */
    public function isStarted()
    {
        return $this->_started;
    }

    /**
     * Start this Transport mechanism.
     */
    public function start()
    {
    }

    /**
     * Stop this Transport mechanism.
     */
    public function stop()
    {
    }

    /**
     * Set Mailgun domain
     * @param string $domain
     */
    public function setMailgunDomain($domain)
    {
        $this->_mailgunDomain = $domain;
    }

    /**
     * Get Mailgun domain
     * @return string
     */
    public function getMailgunDomain()
    {
        return $this->_mailgunDomain;
    }

    /**
     * Set Mailgun Private API Key
     * @param string $key
     */
    public function setPrivateApiKey($key)
    {
        $this->_privateApiKey = $key;
    }

    /**
     * Get Private Api Key
     * @return string
     */
    public function getPrivateApiKey()
    {
        return $this->_privateApiKey;
    }

    /**
     * Send the given Message.
     *
     * Recipient/sender data will be retrieved from the Message API.
     * The return value is the number of recipients who were accepted for delivery.
     *
     * @param Swift_Mime_Message $message
     * @param string[]  $failedRecipients An array of failures by-reference
     *
     * @throws Swift_TransportException
     * @return int sent messages count
     */
    public function send(Swift_Mime_Message $message, &$failedRecipients = null)
    {
        if (null === $message->getHeaders()->get('To')) {
            throw new Swift_TransportException(
                'Cannot send message without a recipient'
            );
        }

        $mgClient = Mailgun::create($this->getPrivateApiKey());
        $result = $mgClient->messages()->send(
            $this->getMailgunDomain(),
            $this->getPostData($message)
        );

        return $result->getId() ? 1 : 0;
    }

    /**
     * Register a plugin in the Transport.
     *
     * @param \Swift_Events_EventListener $plugin
     */
    public function registerPlugin(Swift_Events_EventListener $plugin)
    {
    }

    /**
     * Looks at the message headers to find post data.
     *
     * @param Swift_Mime_Message $message
     *
     * @return array $postData
     */
    protected function getPostData(Swift_Mime_Message $message)
    {
        // get "form", "to" etc..
        $postData = $this->prepareRecipients($message);
        $mailgunHeaders = self::getMailgunHeaders();
        $messageHeaders = $message->getHeaders();

        foreach ($mailgunHeaders as $headerName) {
            /** @var \Swift_Mime_Headers_MailboxHeader $value */
            if (null !== $value = $messageHeaders->get($headerName)) {
                $postData[$headerName] = $value->getFieldBody();
                $messageHeaders->removeAll($headerName);
            }
        }
        $postData['html'] = $message->getBody();
        $postData['subject'] = $message->getSubject();

        return $postData;
    }

    /**
     * @param Swift_Mime_Message $message
     *
     * @return array
     */
    protected function prepareRecipients(Swift_Mime_Message $message)
    {
        $headerNames = ['from', 'to', 'bcc', 'cc'];
        $messageHeaders = $message->getHeaders();
        $postData = [];

        foreach ($headerNames as $name) {
            /** @var \Swift_Mime_Headers_MailboxHeader $h */
            $h = $messageHeaders->get($name);
            $postData[$name] = $h === null ? [] : $h->getAddresses();
        }

        // Merge 'bcc' and 'cc' into 'to'.
        $postData['to'] = array_merge($postData['to'], $postData['bcc'], $postData['cc']);
        unset($postData['bcc']);
        unset($postData['cc']);

        // Remove Bcc to make sure it is hidden
        $messageHeaders->removeAll('bcc');

        return $postData;
    }

    /**
     * Get the special o:* headers. https://documentation.mailgun.com/api-sending.html#sending.
     *
     * @return array
     */
    public static function getMailgunHeaders()
    {
        return ['o:tag', 'o:campaign', 'o:deliverytime', 'o:dkim', 'o:testmode', 'o:tracking', 'o:tracking-clicks', 'o:tracking-opens'];
    }
}
